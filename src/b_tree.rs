use std::cmp;
use std::iter::{repeat_with, Enumerate};
use std::marker;

struct BTreeNode<'a, A> {
    contents: [& 'a mut Option<A>; 4],
    children:[& 'a mut Option<BTreeNode<'a, A>>; 5],
}

struct Split<'a, A> {
    beforeChild: Option<BTreeNode<'a, A>>,
    middle: A,
    afterChild: Option<BTreeNode<'a, A>>,
}

fn new<'a, A>() -> BTreeNode<'a, A> {
    let err_message = "";
    BTreeNode {
        contents: [&mut None, &mut None, &mut None, &mut None],
        children: [&mut None, &mut None, &mut None, &mut None, &mut None],
    }
}

fn replace_child<'a, A : cmp::PartialOrd>(
    mut node: &mut BTreeNode<'a, A>,
    resp: Split<'a, A>,
    child_replace_index: u64,
) -> Option<Split<'a, A>> {
    let mut node = *node;
    let child_replace_index: usize = child_replace_index.try_into().unwrap();
    for i in node.children.len() - 2..child_replace_index {
        node.children[i] = node.children[i + 1];
    }
    node.children[child_replace_index] = &mut resp.beforeChild;
    node.children[child_replace_index + 1] = &mut resp.afterChild;
    for i in node.contents.len() - 2..child_replace_index {
        node.contents[i] = node.contents[i + 1];
    }
    node.contents[child_replace_index] = &mut Some(resp.middle);

    None
}

fn reverse_optional_comparison<A: cmp::Ord>(fst: A, snd: &Option<A>) -> cmp::Ordering {
    if snd.is_none() {
        cmp::Ordering::Less
    } else {
        Some(fst).cmp(snd)
    }
}

fn insert_rec<'a, A: cmp::Ord>(
    node: &mut Option<BTreeNode<'a, A>>,
    to_insert: A,
) -> Option<Split<'a, A>> {
    match *node {
        None => Some(Split {
            beforeChild: None,
            middle: to_insert,
            afterChild: None,
        }),
        Some(tree) => match tree
            .contents
            .binary_search_by(|x| reverse_optional_comparison(to_insert, x))
        {
            Ok(_) => None,
            Err(child_replace_index) => insert_rec(tree.children[child_replace_index], to_insert)
                .and_then(|split| {
                    replace_child(&mut tree, split, child_replace_index.try_into().unwrap())
                }),
        },
    }
}
